var url = require('url');
var UserModel = require('../models/user');
var jwt = require('jwt-simple');
var config = require('../config/database');

module.exports = function(req, res, next){
    var parsed_url = url.parse(req.url, true);
    var token = (req.body && req.body.access_token) || parsed_url.query.access_token || req.headers["x-access-token"];

    if (token) {
        try {
            var decoded = jwt.decode(token, config.secret);
            if (decoded.exp <= Date.now()) {
                res.end('Access token has expired', 400);
            }

            UserModel.findOne({ '_id': decoded.iss }, function(err, user){
                if (!err) {
                    req.user = user;
                    return next();
                }
            });
        } catch (err) {
            return next();
        }
    } else {
        next();
    }
};
