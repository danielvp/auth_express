var express = require('express');
var router = express.Router();

var url;
var mongoose ;
var passport ;
var jwt ;
var validator ;
var assert ;
var moment ;
var jwtAuth;

var config ;
var utils ;

var requireAuth;

var db ;
var userSchema ;
var studentSchema;
var User = userSchema;
var Student = studentSchema;
var ObjectId ;
var id1;

module.exports.setUp = function(url_, mongoose_, passport_,
    jwt_simple_, validator_, assert_, moment_,
    jwt_auth_, database_config_, http_request_,
    require_auth_){
    url = url_;
    mongoose = mongoose_;
    passport = passport_;
    jwt = jwt_simple_;
    validator = validator_;
    assert = assert_;
    moment = moment_;
    jwtAuth = jwt_auth_;

    config = database_config_;
    utils = http_request_;

    requireAuth = require_auth_;

    db = mongoose.connection;
    userSchema = require('./../../models/user');
    studentSchema = require('./../../models/student');
    User = userSchema;
    Student = studentSchema;
    ObjectId = mongoose.Types.ObjectId;
    id1 = new ObjectId();

    router.post('/signup', function(req, res) {
        if (!req.body.user.email || !req.body.user.password) {
            res.json({success: false, msg: 'Please pass email and password.'});
        } else {

            if(!validator.isEmail(req.body.user.email)){
                return res.json({success: false, msg: 'Your email is not valid.'});
            }
            if(!validator.isLength(req.body.user.password, {min:6, max: 16})){
                return res.json({success: false, msg: 'Your password must be between 6 and 16 characters.'});
            }
            if(!validator.isAlphanumeric(req.body.user.password)){
                return res.json({success: false, msg: 'Your password must contain only letters and numbers.'});
            }

            var newUser = new User({
                email: req.body.user.email,
                password: req.body.user.password,
                firstName: req.body.user.firstName,
                lastName: req.body.user.lastName,
                country: req.body.user.country,
                university: req.body.user.university
            });
            newUser.save(function(err) {
                if (err) {
                    return res.json({success: false, msg: JSON.stringify(err)});
                }
                res.json({success: true, msg: 'Successful created new user.'});
            });
        }
    });

    router.post('/signin', function(req, res){
        User.findOne({
            email: req.body.user.email
        }, function(err, user){
            if(err) throw err;

            if(!user){
                res.json({success: false, msg: JSON.stringify(err)});
            }else{
                user.comparePassword(req.body.user.password, function(err, isMatch){
                    if(isMatch && !err){
                        var expires = moment().add('days', 7).valueOf();
                        var token = jwt.encode({ iss: user.id, exp: expires},  config.secret);
                        var jsonResponse = {success: true, token: 'JWT ' + token};
                        jsonResponse.email = user.email;
                        jsonResponse.firstName = user.firstName;
                        jsonResponse.lastName = user.lastName;
                        jsonResponse.country = user.country;
                        jsonResponse.university = user.university;
                        res.json(jsonResponse);
                    }else{
                        res.json({success: false, msg: 'Signin failed, wrong password'});
                    }
                });
            }
        });
    });

    module.exports.router = router;
};
