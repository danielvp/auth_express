var express = require('express');
var router = express.Router();

var url;
var mongoose ;
var passport ;
var jwt ;
var validator ;
var assert ;
var moment ;
var jwtAuth;

var config ;
var utils ;

var requireAuth;

var db ;
var userSchema ;
var studentSchema;
var User = userSchema;
var Student = studentSchema;
var ObjectId ;
var id1;

module.exports.setUp = function(url_, mongoose_, passport_,
    jwt_simple_, validator_, assert_, moment_,
    jwt_auth_, database_config_, http_request_,
    require_auth_){
    url = url_;
    mongoose = mongoose_;
    passport = passport_;
    jwt = jwt_simple_;
    validator = validator_;
    assert = assert_;
    moment = moment_;
    jwtAuth = jwt_auth_;

    config = database_config_;
    utils = http_request_;

    requireAuth = require_auth_;

    db = mongoose.connection;
    userSchema = require('./../../models/user');
    studentSchema = require('./../../models/student');
    User = userSchema;
    Student = studentSchema;
    ObjectId = mongoose.Types.ObjectId;
    id1 = new ObjectId();

    router.get('/student/list/by/university', jwtAuth, requireAuth, function(req, res){
        var parsed_url = url.parse(req.url,true);
        var responseObject = { status: false, universities: [] };

        if(parsed_url.query.university){

            Student.find({'universities.name': parsed_url.query.university }, function(err, students){
                if(err) throw err;
                responseObject.universities = students;
                res.json(responseObject);
            });
        }
        else
        {
            res.json(responseObject);
        }
    });

    router.put('/student', jwtAuth, requireAuth, function(req, res){
        var responseObject = { error: true };
        if(req.body.student){
            Student.where({'_id': ObjectId(req.body.student._id)})
                .update(
                    {
                        nickName: req.body.student.nickName,
                        firstName: req.body.student.firstName,
                        lastName: req.body.lastName
                    }, function(err){
                        if (err) {
                            console.log(JSON.stringify(err));
                        }else{
                            responseObject.error = false;
                            res.json(responseObject);
                        }

                    });

        }else{
            res.json(responseObject);
        }
    });


    router.post('/student', jwtAuth, requireAuth, function(req, res){
        var responseObject = { error: true };
        if(req.body.student){
            let student = new Student({
                nickName: req.body.student.nickName,
                firstName: req.body.student.firstName,
                lastName: req.body.student.lastName,
                universities: req.body.student.universities
            });

            student.save(function(err){
                if (err) {
                    console.log(JSON.stringify(err));
                }else{
                    responseObject.error = false;
                    res.json(responseObject);
                }
            });
        }else{
            res.json(responseObject);
        }
    });

    router.delete('/student', jwtAuth, requireAuth, function(req, res){
        var parsed_url = url.parse(req.url,true);
        var responseObject = { error: true };
        if(parsed_url.query.nickName){

            Student.remove({'nickName': parsed_url.query.nickName}, function(err){
                if (err) {
                    console.log(JSON.stringify(err));
                    throw err;
                }else{
                    responseObject.error= false;
                    res.json(responseObject);
                }

            });
            res.json(responseObject);
        }
    });


    router.put('/student/university', jwtAuth, requireAuth, function(req, res){
        var responseObject = { status: 200 };
        if(req.body.student){

            Student.where({'nickName': req.body.student.nickName})
                .update({ universities: req.body.student.universities }, function(err){
                    if(err) throw err;
                    else
                        res.json(responseObject);
                });
        }else{
            res.json(responseObject);
        }
    });

    module.exports.router = router;
};
