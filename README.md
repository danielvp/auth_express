### What is this repository for? ###

Simple auth app for angular 2 client

### How do I get set up? ###

Using nodejs v6.10.2
npm install
node server.js --> Express app listening on port ${process.env.port || 3500}

Enjoy
