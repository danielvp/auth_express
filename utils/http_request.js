var http = require('http');
var https = require('https');

exports.getJSON = function(options, onResult){
    
    console.log("restApi::getJSON");

    var http_tool = http;// options.port == 443 ? https : http;

    var req = http_tool.request(options, function(res){
        var output = '';
        console.log(options.host + ":" + res.statusCode);
        res.setEncoding('utf8');

        res.on('data', function(chunk){
            output+=chunk;
        });

        res.on('end', function(){
            var obj = JSON.parse(output);
            onResult(res.statusCode, obj);
        });
    });

    req.on('error', function(err){
        console.log("ERERER "+ err.message);
    // res.send("ERRROROROORORO "+ err.message);
    });

    req.end();
};
