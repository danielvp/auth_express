var express = require('express');
var url = require('url');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var passport = require('passport');
var jwt = require('jwt-simple');
var validator = require('validator');
var assert = require('assert');
var moment = require('moment');
var jwtAuth = require('./auth_token/jwt_auth');

var config = require('./config/database');
var utils = require('./utils/http_request');
var admin_routes = require('./controllers/admin/admin_routes');
var auth_routes = require('./controllers/auth/auth_routes');

require('./config/passport')(passport);

var requireAuth = function(req, res, next) {
    if (!req.user) {
        res.end('Not authorized', 401);
    }else {
        next();
    }
};

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Cache-Control");
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan('dev'));

app.use(passport.initialize());

mongoose.connect(config.database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    auth_routes.setUp(
        url,
        mongoose,
        passport,
        jwt,
        validator,
        assert,
        moment,
        jwtAuth,
        config,
        utils,
        requireAuth
    );
    admin_routes.setUp(
        url,
        mongoose,
        passport,
        jwt,
        validator,
        assert,
        moment,
        jwtAuth,
        config,
        utils,
        requireAuth
    );

    app.use('/api/admin',admin_routes.router);
    app.use('/api', auth_routes.router);
});
app.listen(process.env.PORT || 3500, '139.59.146.61' , function() {
    console.log(`Express app listening on port ${process.env.port || 3500}`);
});
