var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
    nickName: { type: String, unique: true, required: true },
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    universities: {type: Array, required: true}
});


module.exports = mongoose.model('Student', studentSchema);
